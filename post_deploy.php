<?php

function post_deploy()
{
    global $POST_DEPLOY;
    global $current_repository;

    if (isset($current_repository, $POST_DEPLOY[$current_repository])) {
        loginfo('[info] starting postdeploy');
        $config = $POST_DEPLOY[$current_repository];
        post_deploy_expand_config($config);

        loginfo('[info] postdeploy: ' . json_encode($config));

        post_deploy_exec($config);
        post_deploy_file_rm($config);
        post_deploy_recursive_rm($config);
    } else {
        if (!isset($current_repository)) {
            loginfo('[error] there is no repository no postdeploy cannot be executed');
        } else {
            loginfo('[info] there isn\'t a postdeploy configuration for the repository ');
        }
    }
}

function post_deploy_resolve_basedir_string($string)
{
    global $current_repository;
    global $DEPLOY; 

    return str_replace('~', $DEPLOY[$current_repository], $string);
}

function post_deploy_exec($config)
{
    if (isset($config['exec'])) {
        foreach ($config['exec'] as $exec) {
            $exec = post_deploy_resolve_basedir_string($exec);
            loginfo('[post_deploy] exec:' . $exec);
            $result = shell_exec($exec);
            $result = explode("\n", $result);
            foreach ($result as $resulti) {
                loginfo(preg_replace('(^)', '[post_deploy][exec]', $resulti));
            }
        }
    }
}

function post_deploy_file_rm($config)
{
    if (isset($config['file_rm'])) {
        foreach ($config['file_rm'] as $pattern) {
            $files = glob(post_deploy_resolve_basedir_string($pattern));
            loginfo('[post_deploy] file_rm:' . $pattern);
            foreach ($files as $file) {
                #loginfo('[post_deploy][file_rm] ' . $file);
                if (is_file($file)) {
                    unlink($file);
                } else {
                    loginfo('[post_deploy][file_rm][error] cannot delete directory ' . $file);
                }
            }
        }
    }
}

function post_deploy_recursive_rm($config)
{
    if (isset($config['recursive_rm'])) {
        foreach ($config['recursive_rm'] as $pattern) {
            $files = glob(post_deploy_resolve_basedir_string($pattern));
            loginfo('[post_deploy] recursive_rm:' . $pattern);
            foreach ($files as $file) {
                loginfo('[post_deploy][recursive_rm] ' . $file);
                if (is_file($file)) {
                    unlink($file);
                } else {
                    _post_deploy_recursive_rm($file);
                }
            }
        }
    }
}

function _post_deploy_recursive_rm($dir)
{
    $files = glob($dir . '/*');

    foreach ($files as $file) {
        if (is_file($file)) {
            unlink($file);
        } else {
            _post_deploy_recursive_rm($file);
        }
    }
}

function post_deploy_expand_config(&$config)
{
    if (isset($config['magento'])) {
        post_deploy_expand_config_magento($config);
    }

    if (isset($config['wordpress'])) {
        post_deploy_expand_config_wordpress($config);
    }

    if (isset($config['concrete'])) {
        post_deploy_expand_config_concrete($config);
    }

    post_deploy_expand_others($config);
}


function post_deploy_expand_config_magento(&$config) 
{
    //default configurations
    $default = array(
        'clear_cache' => true,
        'clear_minified_cssjs' => true,
        'clear_product_image_cache' => true,
        'compile' => false,
        'clear_logs' => false,
    );

    if ($config['magento'] === true || 'magento' === $config || in_array('magento', $config)) {
        $config['magento'] = $default;
    }

    if (isset($config['magento']['clear_cache']) && $config['magento']['clear_cache']) {
        $config['recursive_rm'][] = '~/var/cache/*';
        $config['functions'][] = 'clear_magento_cache';
    }

    if (isset($config['magento']['clear_logs']) && $config['magento']['clear_logs']) {
        $config['recursive_rm'][] = '~/var/log/*';
    }

    if (isset($config['magento']['clear_minified_cssjs']) && $config['magento']['clear_minified_cssjs']) {
        $config['recursive_rm'][] = '~/media/js/*';
        $config['recursive_rm'][] = '~/media/css/*';
    }
    
    if (isset($config['magento']['clear_product_image_cache']) && $config['magento']['clear_product_image_cache']) {
        $config['recursive_rm'][] = '~/media/catalog/product/cache/*';
    }
    
    if (isset($config['magento']['compile']) && $config['magento']['compile']) {
        $config['exec'][] = 'php -f ~/shell/compiler.php -- compile';
    }
}


function post_deploy_expand_config_wordpress(&$config) 
{
    //default configurations
    $default = array(
        'clear_cache' => true,
    );

    if ($config['wordpress'] === true || 'wordpress' === $config || in_array('wordpress', $config)) {
        $config['wordpress'] = $default;
    }

    if (isset($config['wordpress']['clear_cache']) && $config['wordpress']['clear_cache']) {
        $config['recursive_rm'][] = '~/wp-content/cache/*';
    }
}

function post_deploy_expand_config_concrete(&$config)
{
    //default configurations
    $default = array(
        'clear_cache' => true,
    );

    if ($config['concrete'] === true || 'concrete' === $config || in_array('concrete', $config)) {
        $config['concrete'] = $default;
    }

    if (isset($config['concrete']['clear_cache']) && $config['concrete']['clear_cache']) {
        $config['recursive_rm'][] = '~/files/cache/*';
    }

}

function post_deploy_expand_others(&$config)
{
    if ($config['memcached']) {
        $config['exec'][] = '(sleep 2; echo flush_all; sleep 2; echo quit; ) | telnet 127.0.0.1 11211';
    }
}